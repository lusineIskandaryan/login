export class User {
    public id: number;
    public first_name: string;
    public last_name: string;
    public avatar: string;

    constructor(data) {
        this.id = data.id;
        this.first_name = data.first_name;
        this.last_name = data.last_name;
        this.avatar = data.avatar;
    }
}
