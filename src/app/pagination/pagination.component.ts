import { Component, Input, Output, EventEmitter } from '@angular/core';
import { UserService } from '../services/user.service';

@Component({
    moduleId: module.id,
    selector: 'pagination',
    templateUrl: 'pagination.component.html',
    styleUrls: ['pagination.component.css'],
})
export class PaginationComponent {
    @Output() pageUpdated: EventEmitter<any> = new EventEmitter();
    currentPage: number;
    @Input() totalPages: number;
    pages: number[] = [];
    constructor(private service: UserService) {
        this.getPages();
    }

    ngOnInit() { 
    }
    
    private getPages() {
        for (let i = 0; i <= 3; i++){
            this.pages[i] = i;
        } 
    }
    
    sendPage(currentPage) {
        this.pageUpdated.emit(currentPage);
    }
}
     