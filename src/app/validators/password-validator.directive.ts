import { Directive, forwardRef, Attribute } from '@angular/core';
import {Validator, AbstractControl, NG_VALIDATORS} from '@angular/forms';
@Directive ({
    selector:'[passwordValidator]',
    providers:[
        {provide: NG_VALIDATORS, useExisting:forwardRef(()=> PasswordValidator), multi:true}
    ]
})
export class PasswordValidator implements Validator {

    validate(control:AbstractControl): {[key:string]: any} {
        let passwordRegex = /(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}/;
        let value=control.value;
        let result=passwordRegex.test(value);
        if(!result) {
            return {passwordValidator:result};
        }
        return null;
    }
}