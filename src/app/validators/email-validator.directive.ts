import { Directive, forwardRef, Attribute } from '@angular/core';
import {Validator, AbstractControl, NG_VALIDATORS} from '@angular/forms';

@Directive ({
    selector:'[emailValidator]',
    providers:[
        {provide: NG_VALIDATORS, useExisting: EmailValidator, multi:true}
    ]
})
export class EmailValidator implements Validator {

    validate(control:AbstractControl): {[key:string]: any} {
        let emailRegex = /[A-Z0-9._%-]+@[A-Z0-9.-]+\.[A-Z]{2,4}/i;
        let value=control.value;
        let result=emailRegex.test(value);
        if(!result) {
            return {emailValidator:result};
        }
        return null;
    }
}
