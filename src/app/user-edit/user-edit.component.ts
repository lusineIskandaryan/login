import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';
import { User } from '../users/user';

@Component({
  moduleId: module.id,
  selector: 'user-edit',
  templateUrl: 'user-edit.component.html',
  styleUrls: ['user-edit.component.css']
})
export class UserEditComponent implements OnInit {
  curentUser: User;
  errorMessage: string;
  userEditForm: FormGroup;
  constructor(private service:UserService, 
    private activatedRoute: ActivatedRoute,
    private router: Router) { }

  ngOnInit() {
    this.userEditForm=new FormGroup({
       id: new FormControl('',Validators.required),
       first_name: new FormControl('',Validators.required),
       last_name: new FormControl('',Validators.required)
    });

    this.activatedRoute
        .params
        .subscribe(params => {
          this.service.currentUsee(params['id'])
            .subscribe(result => {
              this.curentUser = result.data; console.log('jhbjb')
              delete result.data.avatar
              this.userEditForm.setValue(result.data);
            })
        });
    }
  
  public save(form) {
    this.service.updateUser(form.first_name,form.last_name, form.id).subscribe(
      result => {
         console.log(result); 
         alert('this user successfully updated')
        }); 
      this.router.navigate(['users']);
      }
}