import { Http, Response, RequestOptions } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import { User } from '../users/user';

@Injectable ()
export class UserService{
    private url = 'https://reqres.in/api/users';
    users: User;
    constructor(private http: Http) { }
    
    
    
    public getUsers(page) { 
      return this.http.get(this.url +'?page='+ page)
        .map (response => response.json())
        .catch((error: any) => {  
          return Observable.throw(error.message);
         
        });
    }
    
    public getPages(page){
      return this.http.get(this.url +'?page='+ page)
        .map (response => response.json())
        .catch((error: any) => {  
          return Observable.throw(error.message);
        });
    }
            
    public addUser(first_name: string, last_name: string): Observable<User[]> { 
      return this.http.post(this.url, {first_name, last_name})
        .map(response => response.json())
        .catch((error: any) => {  
          return Observable.throw(error.message);
         
        });
    }
    public currentUsee(id:number) {
      return this.http.get(this.url + '/' + id)
        .map(response => response.json())
        .catch((error: any) => {  
          return Observable.throw(error.message);
        });
    }

    public updateUser(first_name: string, last_name: string, id:number): Observable<User[]> { 
      return this.http.put(this.url + '/' + id, {name:first_name, job:last_name})
        .map(response => response.json())
        .catch((error: any) => {  
          return Observable.throw(error.message);
         
        });
    }

    public deleteContact(id:number){ 
      return this.http.delete(this.url + '/' + id)
      .map(response => response.json())
        .catch((error: any) => {  
          return Observable.throw(error.message);
         
        });
    }
}