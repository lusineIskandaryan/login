import { Http, Response, Headers } from '@angular/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

@Injectable ()
export class LoginService { 
    public token: string;
    userUrl='https://reqres.in/api/login';
    constructor(private http: Http) {
        var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        this.token = currentUser && currentUser.token;
    }

    login(email:string,password:string): Observable<boolean> { 
        const body = JSON.stringify({email:email,password:password});
        let headers = new Headers({'Content-Type': 'application/json'});
        return this.http.post(this.userUrl, body, {headers: headers})
            .map((response: Response) => {
                let token = response.json() && response.json().token;
                if (token) {
                    this.token = token;console.log(token);
                    localStorage.setItem('currentUser', JSON.stringify({ username: email, token: token }));
                    return true;
                } else {
                    return false;
                }
            })
            .catch((error: any) => { 
                return Observable.throw(error.message); 
            });
    }
    
    logout() {
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}

