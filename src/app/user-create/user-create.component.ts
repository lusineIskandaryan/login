import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { UserService } from '../services/user.service';

@Component({
  moduleId: module.id,
  selector: 'user-create',
  templateUrl: 'user-create.component.html',
  styleUrls: ['user-create.component.css']
})
export class UserCreateComponent implements OnInit {
  userCreateForm: FormGroup;
  constructor(private service:UserService) { }

  ngOnInit() {
    this.userCreateForm=new FormGroup({
      first_name: new FormControl('',Validators.required),
      last_name: new FormControl('',Validators.required)
    });
  }

  public save(form) {
    return this.service.addUser(form.first_name,form.last_name)
      .subscribe((result) => {
        document.getElementById('close').click();
        alert('user successfully created')
      },
        error => {
          console.log(error);
        }
      );
  }

  public close () {
  }
}

  
  
    

  

  