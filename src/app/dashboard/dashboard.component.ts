import { Component, Input, OnInit } from '@angular/core';
import { User } from '../users/user';
import { UserService } from '../services/user.service';
import { Http, RequestOptions } from '@angular/http';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { LoginService } from '../services/auth.service';

@Component({
    moduleId: module.id,
    selector: 'dashboard',
    templateUrl: 'dashboard.component.html',
    styleUrls: ['dashboard.component.css'],
})
export class DashboardComponent implements OnInit {
  users: User[];
  currentPage:number =1;
  totalPages:number = 0;
  currentUser: User;

  constructor (private service: UserService, private serviceOut: LoginService,
    private router: Router, private activatedRoute: ActivatedRoute) { }

  getPage(page) {
    this.currentPage = page;
    this.getUsers();
  }

  ngOnInit() { 
    this.getUsers()
    }
    
  getUsers() {
    this.service.getUsers(this.currentPage)
      .subscribe(
        response => {
          this.totalPages=response.total_pages;
          this.users=response.data; 
        },
        error => {console.log(error)}
      );
  } 
      
  public create() {
  }
      
  logOut() {
    this.serviceOut.logout();
    this.router.navigate(['/login']);
  }
      
  public sort(key, asc) {
    this.users.sort((value1,value2)=>{
      if (value1[key] < value2[key]) {
        return asc ? -1 : 1;
      } else if (value1[key] == value2[key] ) {
        return 0
      } else { 
        return asc ? 1: -1;
      }
      });
  }

  public edituser(user:User) {
    this.router.navigate(['/users/edit', user.id]);
  }

  public delete() {
    this.service.deleteContact(this.currentUser.id)
      .subscribe(
        data => {
          document.getElementById('close').click();
          alert('user successfully deleted')
        },
        error => {console.log(error)}
      )         
  }

  public setCurrentUser(user:User) {
    this.currentUser = user;
  }
}
      
  
          

        