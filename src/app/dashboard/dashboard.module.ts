import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './dashboard.component';
import { UserEditComponent } from '../user-edit/user-edit.component';
import { UserCreateComponent } from '../user-create/user-create.component';
import { UserService } from '../services/user.service';
import { PaginationComponent } from '../pagination/pagination.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
  ],
  declarations: [
    DashboardComponent,
    UserEditComponent,
    UserCreateComponent,
    PaginationComponent
  ],
  exports: [ DashboardComponent ],
  providers: [
    UserService,
  ],
})
export class DashboardModule { }
