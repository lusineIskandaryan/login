import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Http, RequestOptions } from '@angular/http';
import { Router } from '@angular/router';
import { PasswordValidator } from '../validators/password-validator.directive';
import { EmailValidator } from '../validators/email-validator.directive';
import { LoginService } from '../services/auth.service';

@Component({
    moduleId: module.id,
    selector: 'login',
    templateUrl: 'login.component.html',
    styleUrls: ['login.component.css'],
})
export class LoginComponent implements OnInit {
    userForm: FormGroup;
    formErrors = {
        'email': '',
        'password': '',
    };
    validationsMessages = {
        'email': {'required' : 'email is required.', 'EmailValidator': 'it must be email addres'},
        'password': {'required' : 'password is required.', 'EmailValidator' : 'password must contain min 8 characters, includind UPPER/lowercase and number or symbol'}
    };
    constructor(private fb: FormBuilder ,private service: LoginService, 
        private router: Router) {
    
    }
        
    ngOnInit() {
        this.createForm()
        this.service.logout();
    }

    createForm() {
        this.userForm =this.fb.group({
            email:['',[Validators.required, EmailValidator]],
            password: ['',[Validators.required, PasswordValidator]],
        });
    }
    onSubmit(form) { 
        this.service.login(form.email,form.password)
            .subscribe( result => {
                if(result) {
                    this.router.navigate(['/users']);
                }
            err => err.json().message
        })
    }

    onValueChanged(data?: any) {
        if (!this.userForm) { return; }
        let form = this.userForm;

        for (let field in this.formErrors) {
            this.formErrors[field] = '';
            let control = form.get(field);

            if (control && control.dirty && !control.valid) {
                let messages = this.validationsMessages[field];
                for (let key in control.errors) {
                    this.formErrors[field] += messages[key] + ' ';
                }
            }
        }
    }
} 