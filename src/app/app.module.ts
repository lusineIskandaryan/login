import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { DashboardModule } from './dashboard/dashboard.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { EmailValidator } from './validators/email-validator.directive';
import { PasswordValidator } from './validators/password-validator.directive';
import { LoginService } from './services/auth.service';
import { AppRoutingModule } from './app-routing.module';
import { AuthGuard } from './services/guard.service';
import { PageNotFoundComponent } from './page-not-found/not-found.component';

@NgModule({
  declarations: [
    AppComponent, 
    LoginComponent, 
    EmailValidator, 
    PasswordValidator,
    PageNotFoundComponent,
  ],
  imports: [
    BrowserModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    AppRoutingModule,
    DashboardModule
  ],
  providers: [
    LoginService,
    AuthGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
